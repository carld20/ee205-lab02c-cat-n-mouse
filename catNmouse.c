///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Carl Domingo <carld20@hawaii.edu>
/// @date    02/23/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
	int theMaxValue = atoi(argv[1]);
	if (theMaxValue >= 1){
		int aguess;
		int theNumberImThinkingOf = rand() % theMaxValue;
	do{
		printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess: \n", theMaxValue);
		scanf("%d", &aguess);
		if (aguess == theNumberImThinkingOf){
			printf("You got me.\n");
			printf("|\\---/|\n");
			printf("| o_o |\n");
			printf( " \\_^_/\n");
			return 0;
		}
		else if (aguess < 1){
			printf("You must enter a number that's >= 1\n");
		}
		else if (aguess > theMaxValue){
			printf("You must enter a number that's <= %d\n", theMaxValue);
		}
		else if (aguess > theNumberImThinkingOf){
			printf("No cat... the number I'm thinking of is smaller than %d\n", aguess);
		}
		else if (aguess < theNumberImThinkingOf){
			printf("No cat... the number I'm thinking of is larger than %d\n", aguess);
		}
	}while (aguess != theNumberImThinkingOf);
}
	else{
		printf("ERROR NUMBER MUST BE >= 1\n");
		return 1;
	}
}
